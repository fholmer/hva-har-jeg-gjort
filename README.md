# Oversikt #

Sliter du med å føre timelister? Kan du ikke huske hva du gjorde mandag?

Dette programmet løser ikke problemet ditt, men det holder deg i hånda.

### Hvordan kommer jeg i gang? ###

Først installér python 3.4 eller nyere. Deretter må du installere to tilleggbiblioteker:

* [pywin32](https://sourceforge.net/projects/pywin32/files/)
* [pytz](https://pypi.python.org/pypi/pytz)

pywin32 må lastes ned fra sourceforge og installeres. Men pytz kan enkelt installeres fra kommandolinje slik:

    # I Windows:
    py -3 -m pip install pytz

    # I linux
    python3 -m pip install pytz


Så kan du klone denne repoen:

    cd MINFAVORITTMAPPE
    hg clone https://fholmer@bitbucket.org/fholmer/hva-har-jeg-gjort "Hva har jeg gjort"
    cd "Hva har jeg gjort"

Nå er det bare å dobbeltklikke på "Hva har jeg gjort.py"

### Konfigurering ###

For å fortelle hvilke mapper som skal søkes etter endringer må du lage en konfigfil.
kopier filen config/default.conf og gi den navnet local.conf.

Rediger config/local.conf og legg til mappene dine:

    [dirlist]
    ..
    C:\Users\fillefrans\Documents
    C:\Prosjekt
    D:\Prosjekt

