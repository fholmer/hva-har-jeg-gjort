from contextlib import contextmanager
from .dataflyt import Tittel, Paragraf, Tabell

class NewPage():
    def __init__(self, parent, title):
        self.parent = parent
        self.title = title
        
    def __enter__(self):
        title = self.title
        self.parent.append_nav('<li><a href="#{0}">{0}</a></li>'.format(title))
        self.parent.append_table('\n<div><h1 id="{0}">{0}</h1>\n<table>'.format(title))
        print("")
        print (title)
        print("")
        
    def __exit__(self, *err):
        self.parent.append_table('\n</table></div>')

class Bootstrap():
    def __init__(self, template):
        self.nav_list = ""
        self.div_table = ""
        self.header_title = ""
        self.load(template)
  
    def load(self, template):
        self.template = template

    def dump(self):
        template = self.template
        template = template.replace("{_HEADER_TITLE_}", self.header_title)
        template = template.replace("{_NAV_LIST_}", self.nav_list)
        template = template.replace("{_DIV_TABLE_}", self.div_table)
        return template

    def append(self, data):
        if isinstance(data, Tabell):
            self.append_table_row(data.data)
        else:
            self.append_table_row(data)
        
    def append_nav(self, txt):
        self.nav_list += txt

    def append_table(self, txt):
        self.div_table += txt
        
    def append_header_title(self, header_title):
      self.header_title = header_title

    def new_page(self, title):
        return NewPage(self, title)
        
    def append_table_row(self, msg):
        if isinstance(msg, tuple):
            print("    ".join(map(str, msg)))
            msg = " ".join('\n\t\t<td>{}</td>'.format(x) for x in msg)
        elif msg == "":
            print("")
            msg = '\n\t\t<td colspan="4"> </td>'
        else:
            print(msg)
            msg = '\n\t\t<th colspan="4">{}</th>'.format(msg)
            
        self.div_table += "\n\t<tr>{}</tr>".format(msg)
