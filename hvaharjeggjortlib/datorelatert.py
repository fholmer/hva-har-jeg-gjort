#! /usr/bin/env python3
import time
from datetime import datetime, timedelta
import pytz

def hentDatoSomInt(spør_bruker=True, dag=None, mnd=None, år=None, relativ=False):
    #default dato
    now = datetime.now()    
    
    if relativ:
        relativedays = 0
        if år:
            relativedays += (år * 365)
        if mnd:
            relativedays += int(mnd * (365.0 / 12.0))
        if dag:
            relativedays += dag
        relative_now = now + timedelta(days=int(relativedays))
        år, mnd, dag = relative_now.year, relative_now.month, relative_now.day
      
    else:
        #hvis ikke dag, mnd eller år er satt så bruker vi dato i dag
        if not år:
            år = now.year
        if not mnd:
            mnd = now.month
        if not dag:
            dag = now.day
    
    if spør_bruker:
        # formater dag,mnd år som tekst slik at vi kan spørre bruker og bruke det som en del av filnavn
        default_dag, default_mnd, default_år = formaterDatoSomTekst(dag, mnd, år)

        # hente dato fra input
        brukervalgt_år = input("År [%s] :" % default_år)
        brukervalgt_mnd = input("Måned [%s] :" % default_mnd)
        brukervalgt_dag = input("Dag [%s] :" % default_dag)
        
        # hvis bruker trykket enter så bruk default i stedet.
        år = brukervalgt_år or default_år
        mnd = brukervalgt_mnd or default_mnd
        dag = brukervalgt_dag or default_dag
        
    return int(dag), int(mnd), int(år)


def formaterDatoSomTekst(dag, mnd, år):
    return "%02d" % int(dag), "%02d" % int(mnd), "%04d" % int(år)


def hentTidsromSomDatetime(dag, mnd, år):
    today_start = datetime(int(år), int(mnd), int(dag), tzinfo=pytz.utc)
    today_end = datetime(int(år), int(mnd), int(dag), 23, 59, 59, tzinfo=pytz.utc)
    return today_start, today_end


def hentTidsromSomDatetimeUtenTz(dag, mnd, år):
    today_start = datetime(int(år), int(mnd), int(dag))
    today_end = datetime(int(år), int(mnd), int(dag), 23, 59, 59)
    return today_start, today_end


def hentDateTimeSomInt(datetimedata):
    år, mnd, dag = datetimedata.year, datetimedata.month, datetimedata.day
    return dag, mnd, år


