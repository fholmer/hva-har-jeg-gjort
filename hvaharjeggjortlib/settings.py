#! /usr/bin/env python3
from configparser import ConfigParser
import os

# For at vi skal kunne liste opp mapper under [dirlist] og [repolist]
# må vi ikke forvente en "= verdi" etter navnet (som i andre options).
# Derfor allow_no_value=True
# Og ettersom : er mye brukt i filnavn på windows (C:\, D:\) så har jeg begrenset 
# delimiters til "=". default er at både = og : er gyldig
settings = ConfigParser(allow_no_value=True, delimiters="=")

if os.name == 'posix':
  # Ettersom jeg bruker option-nøkkel til å liste opp mappenavn i [dirlist]
  # så må nøklene være case-sensitive i linux. i windows betyr ikke dette noe.
  # Dette trikset gjør at option-nøkkel forblir case-sensitive
  settings.optionxform = str

# finn mappen som config og startbootstrap er plassert. vi kaller mappen root_dir
root_dir = os.path.dirname(os.path.dirname(__file__))

# Her leser vi konfigfilene.
settings.read(os.path.join(root_dir, 'config','default.conf'))
settings.read(os.path.join(root_dir, 'config','local.conf'))
settings.read(os.path.join(root_dir, 'config','local.{}.conf'.format(os.name)))
