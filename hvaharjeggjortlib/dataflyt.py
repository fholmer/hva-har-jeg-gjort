
class Tittel():
    def __init__(self, tittel):
        self.tittel = str(tittel)
    def __str__(self):
        return self.tittel

class Paragraf():
    def __init__(self, paragraf):
        self.paragraf = str(paragraf)
    def __str__(self):
        return self.paragraf

class Tabell():
    def __init__(self, index, data):
        self.data = tuple(map(str, data))
        self.index = data[index]
    def __str__(self):
        return "    ".join(self.data)
