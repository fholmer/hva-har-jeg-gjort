#! /usr/bin/env python3

import os
from os.path import getmtime, join as pjoin, abspath
from datetime import datetime
import pytz

from .settings import settings
from .datorelatert import hentTidsromSomDatetime

def scan_alt(dag, mnd, år):
    """En generator som søker gjennom undermapper og list opp filer som er endret.
    Vi yielder resultatet som tekst"""
    
    today_start, today_end = hentTidsromSomDatetime(dag, mnd, år)

    prev = ""
    
    ignoredirlist = list(settings['ignoredirlist'])
    ignoreVCS = settings['scanfolders'].getboolean('ignoreVCS')
    
    for dirpath in settings['dirlist']:
        if os.path.exists(dirpath):
            dirpath = abspath(dirpath)
            print("Søker gjennom", dirpath)
            for root, dirs, files in os.walk(dirpath):
                # ignorerer mapper listet opp i [ignoredirlist]
                for ignoredir in ignoredirlist:
                    if ignoredir in dirs:
                        dirs.remove(ignoredir)

                if ignoreVCS:
                    # Ignorerer .hg repoer
                    # Endringer her fanges opp av scan_hg_repo-modulen
                    if '.hg' in dirs:
                        dirs.clear() 
                        files.clear()
                for filename in files:
                    try:
                        filetime = datetime.fromtimestamp(getmtime(pjoin(root, filename)), tz=pytz.utc)
                        if today_end > filetime > today_start:
                            if not prev == root:
                                prev = root
                                yield(root)
                            yield(filetime.strftime("%H:%M"), filename)
                            
                    except FileNotFoundError:
                        pass
                        # dersom vi ikke har rettigheter til filen så får vi denne
                        # feilen
                    except OSError as e:
                        if e.errno == 40:
                            pass
                            # Vi hopper over denne feilen:
                            #    OSError: [Errno 40] Too many levels of symbolic links
                        else:
                          raise (e)
