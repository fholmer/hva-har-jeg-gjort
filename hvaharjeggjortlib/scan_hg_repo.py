#! /usr/bin/env python3
import os
import sys
import subprocess
import xml.etree.ElementTree as ET

from .settings import settings
from .datorelatert import formaterDatoSomTekst
from .dataflyt import Tittel, Paragraf, Tabell

def scan_alt(dag, mnd, år):
    """En generator som søker gjennom alle hg-repo og returnerer changelog for valgt dato.
    Vi yielder resultatet som tekst"""

    # lage dato-søkestreng for hg log -d kommandoen.
    dato = "-".join(formaterDatoSomTekst(dag, mnd, år)[::-1])
    
    prev = ""
  
    repo_list = [] ## liste over alle repo-roots som skal undersøkes

    # finne repo-listen i tortoisehg
    if settings['tortoisehg'].getboolean('parserepos'):
      if os.name == 'nt':
        repofile = os.path.expandvars("%APPDATA%/TortoiseHg/thg-reporegistry.xml")
      else:
        repofile = os.path.expanduser("~/.config/TortoiseHg/thg-reporegistry.xml")
        
      if not os.path.isfile(repofile):
          print ("tortoisehg er ikke installert")
          return
      # søk gjennom XML-DOM etter alle repo 
      xmlroot = ET.parse(repofile).getroot()
      for i in xmlroot[0]:
        for j in i:
          repo_list.append(j.attrib["root"])
      
    for repo_root in settings['repolist']:
      repo_list.append(repo_root)
    
    hg_cmd = settings["tortoisehg"]["hg_cmd"]
    hg_args = settings["tortoisehg"]["hg_args"].split(" ")

    # kjør hg log -d dato på alle repoene
    for repo_root in repo_list:
      if os.path.isdir(os.path.join(repo_root, ".hg")):
        
        for line in subprocess.check_output(
                    [hg_cmd,
                    "-R",
                    repo_root,
                    'log',
                    '-d',
                    dato,
                    '--template', '{date|isodate}\t{rev}\t{date|age}\t{author|user}\t{desc}\r\n'
                ] + hg_args).decode(
                    sys.getfilesystemencoding()
                ).splitlines():
            
            if not prev == repo_root:
                  prev = repo_root
                  yield Paragraf(repo_root)
                  
            yield Tabell(0, line.split("\t"))
