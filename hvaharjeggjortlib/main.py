#! /usr/bin/env python3
import os
from importlib import util as importutil
from .datorelatert import hentDatoSomInt, formaterDatoSomTekst
from . import scan_hg_repo, scan_folders, scan_outlook, scan_eventlog
from .bootstrap import Bootstrap
from .settings import settings, root_dir

def main(spør_bruker=True, dag=None, mnd=None, år=None, relativ=False):

    dag, mnd, år = hentDatoSomInt(spør_bruker, dag, mnd, år, relativ)
    
    bootstrap_template = os.path.join(root_dir, settings["bootstrap"]["template"])
    bootstrap = Bootstrap(open(bootstrap_template).read())
    
    title = "{}-{}-{}".format(*formaterDatoSomTekst(dag, mnd, år)[::-1])
    bootstrap.append_header_title(title)

    with bootstrap.new_page("Repo"):
        for resultat in scan_hg_repo.scan_alt(dag, mnd, år):
            bootstrap.append(resultat)

    with bootstrap.new_page("Filer"):
        for resultat in scan_folders.scan_alt(dag, mnd, år):
            bootstrap.append(resultat)

    if importutil.find_spec("win32com"):
      
        with bootstrap.new_page("Outlook"):
            for resultat in scan_outlook.scan_alt(dag, mnd, år):
                bootstrap.append(resultat)
                
        with bootstrap.new_page("Eventlog"):
            for resultat in scan_eventlog.scan_alt(dag, mnd, år):
                bootstrap.append(resultat)
        
        #for resultat in scan_onenote.scan_alt(dag, mnd, år):
        #    print(resultat)
    else:
        print("Kan ikke scanne outlook")
        print("Kan ikke scanne eventlog")
    
    filnavn = 'logg.{}.{}.html'.format(title, os.name)
    f = open(os.path.join(root_dir, filnavn), "w")
    f.write(bootstrap.dump())
    
    return True

if __name__ == '__main__':
    main()
