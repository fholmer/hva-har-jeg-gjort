#! /usr/bin/env python3


try:
    import win32evtlog
    import win32evtlogutil
    #import winerror
except:
    pass
 
from .datorelatert import hentTidsromSomDatetimeUtenTz

def scan_alt(dag, mnd, år):
    """Søker gjennom epost og kalender i outlook.
    Yielder resultatet som tekst"""
    
    today_start, today_end = hentTidsromSomDatetimeUtenTz(dag, mnd, år)
    
    logtypes = {
        #'Application':['ignore'],
        #'Secutity':['ignore'],
        'System':['EventLog']
        }

    for logtype, sources in logtypes.items():
        hand = win32evtlog.OpenEventLog('localhost', logtype)
        total = win32evtlog.GetNumberOfEventLogRecords(hand)
        print ("    Totalt antall eventer i %s = %s" % (logtype, total))
        flags = win32evtlog.EVENTLOG_BACKWARDS_READ|win32evtlog.EVENTLOG_SEQUENTIAL_READ
        
        events=1
        while events:
            events=win32evtlog.ReadEventLog(hand,flags,0)

            for ev_obj in events:
                event_time = ev_obj.TimeGenerated  #.Format() #'12/23/99 15:54:09'
                if today_end > event_time > today_start:
                    msg = win32evtlogutil.SafeFormatMessage(ev_obj, logtype).replace("\r\n", "")
                    if sources:
                        for source in sources:
                            if ev_obj.SourceName == source:
                                yield (event_time, ev_obj.EventType, ev_obj.EventCategory, ev_obj.SourceName, msg)
                    else:
                        yield (event_time, ev_obj.EventType, ev_obj.EventCategory, ev_obj.SourceName, msg[:60])
                
                
                
if __name__ == '__main__':
    # for debugging
    from .datorelatert import hentDatoSomInt
    d, m, å = hentDatoSomInt(False, dag=-1, relativ=True)
    print(d, m, å)
    for l in scan_alt(d, m, å):
      print (l)