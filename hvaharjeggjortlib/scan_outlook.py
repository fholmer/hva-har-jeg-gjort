#! /usr/bin/env python3

from  importlib import util as importutil
if importutil.find_spec("win32com"):
    import win32com.client

from .settings import settings
from .datorelatert import hentTidsromSomDatetime


def scan_alt(dag, mnd, år):
    """Søker gjennom epost og kalender i outlook.
    Yielder resultatet som tekst"""
    
    today_start, today_end = hentTidsromSomDatetime(dag, mnd, år)

    outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
    
    # olFolderDrafts 16 The Drafts folder.    # olFolderInbox 6 The Inbox folder.    #olFolderSentMail 5 The Sent Mail folder.
    #https://msdn.microsoft.com/en-us/library/bb208072.aspx
    #https://support.microsoft.com/en-us/kb/313795    #https://msdn.microsoft.com/en-us/library/office/ff869597.aspx
    #https://msdn.microsoft.com/en-us/library/office/ff866278.aspx    #https://msdn.microsoft.com/en-us/library/office/ff861252.aspx
    #https://msdn.microsoft.com/en-us/library/office/ff862177.aspx
    
    prev = ""

    for id, name in ((6, "Innboks"), (5, "Sendte elementer")):
        print("Scanning", name)    
        inbox = outlook.GetDefaultFolder(id)
        items = inbox.Items
        datefilter = "[ReceivedTime] > '{}' And [ReceivedTime] < '{}'".format(today_start.strftime("%d/%m/%Y %H:%M"), today_end.strftime("%d/%m/%Y %H:%M"))
        items = items.Restrict(datefilter)

        for i in range(items.Count):
            item = items.Item(i+1)
            
            if not prev == name:
                prev = name
                yield("Outlook " + name)
                            
            yield("Emne : " + item.Subject)
            yield("","Dato : " , str(item.ReceivedTime))
            try:
              #godtatte møteinnkallelser har ikke Fra/Til/Kopi-attributt
              yield("","Fra  : " , str(item.Sender))
              yield("","Til  : " , str(item.To))
              yield("","Kopi : " , str(item.CC))
            except AttributeError:
              pass
            yield("")

    print("Scanning", "Calendar")    
    appointments = outlook.GetDefaultFolder(9)
    items = appointments.Items
    #items.IncludeRecurrences = True
    #items.Sort("[Start]", True)
    datefilter = "[Start] > '{}' And [Start] < '{}'".format(today_start.strftime("%d/%m/%Y %H:%M"), today_end.strftime("%d/%m/%Y %H:%M"))
    items = items.Restrict(datefilter)
    
    name = "Calendar"
    for i in range(items.Count):
        item = items.Item(i+1)
        if item is None:
            print(i)
            continue
        
        if not prev == name:
            prev = name
            yield("Outlook " + name)
            yield("")
            
        #if item.IsRecurring:
        #    rp = item.GetRecurrencePattern()

        yield("Emne : " + item.Subject)
        yield("", "Dato : " , str(item.Start))
        yield("", "Deltakere: " , str(item.RequiredAttendees))
        yield("")
